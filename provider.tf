provider "google" {
   version = "1.20.0"
   credentials = "${file("./creds/serviceaccount.json")}"
   project     = "project-devops-314516" # REPLACE WITH YOUR PROJECT ID
   region      = "europ-west1"
 }
